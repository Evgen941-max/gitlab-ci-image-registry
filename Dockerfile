FROM python:latest
COPY Calculator.py .
COPY testCalculator.py .
CMD ["python", "testCalculator.py", "-v"]
